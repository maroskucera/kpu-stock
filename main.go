package main

import (
	"embed"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"log"
	"math"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	bolt "go.etcd.io/bbolt"
)

type StockCategory struct {
	Name string
	Slug string
}

type TemplateData struct {
	Title          string
	Categories     []StockCategory
	HalvesEnable   bool
	QuartersEnable bool
}

type modifyOp int

type modifyRequestData struct {
	Category  string   `json:"category"`
	Operation modifyOp `json:"operation"`
}

type modifyResponseData struct {
	Category string  `json:"category"`
	Value    float64 `json:"value"`
}

const (
	ModifyAdd modifyOp = iota
	ModifySub
	ModifyAddHalf
	ModifySubHalf
	ModifyAddQuarter
	ModifySubQuarter
)

// categories are defined in categories.go

//go:embed templates/*
var templates embed.FS

func main() {
	port := os.Getenv("KPU_STOCK_PORT")
	if port == "" {
		port = "8080"
	}
	mux := &http.ServeMux{}
	server := &http.Server{
		Addr:         fmt.Sprintf("127.0.0.1:%s", port),
		Handler:      mux,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
	}

	db, err := bolt.Open("data", 0666, nil)
	if err != nil {
		log.Fatalf("error while opening DB: %s", err)
	}
	defer db.Close()

	err = db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte("output"))
		return err
	})
	if err != nil {
		log.Fatalf("error while setting up DB buckets: %s", err)
	}

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		templateData := TemplateData{
			Title:          "KPU Sklad",
			Categories:     categories,
			HalvesEnable:   true,
			QuartersEnable: true,
		}

		tpl, err := template.ParseFS(templates, "templates/index.gohtml")
		if err != nil {
			log.Printf("error parsing template files: %s", err)
			w.WriteHeader(http.StatusInternalServerError)
			_, err := w.Write([]byte(http.StatusText(http.StatusInternalServerError)))
			if err != nil {
				log.Printf("error writing to output: %s", err)
			}
			return
		}

		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "text/html")

		err = tpl.Execute(w, templateData)
		if err != nil {
			log.Printf("error writing to output: %s", err)
		}
	})

	mux.HandleFunc("/init.json", func(w http.ResponseWriter, r *http.Request) {
		data := make([]modifyResponseData, len(categories))
		err = db.View(func(tx *bolt.Tx) error {
			bucket := tx.Bucket([]byte("output"))
			for index, category := range categories {
				value, err := strconv.ParseFloat(string(bucket.Get([]byte(category.Slug))), 64)
				if err != nil {
					log.Printf("loaded nil value from DB for key: %s", category.Slug)
				}
				data[index] = modifyResponseData{
					Category: category.Slug,
					Value:    value,
				}
			}
			return nil
		})
		if err != nil {
			log.Printf("error loading from database: %s", err)
			w.WriteHeader(http.StatusInternalServerError)
			_, err := w.Write([]byte(http.StatusText(http.StatusInternalServerError)))
			if err != nil {
				log.Printf("error writing to output: %s", err)
			}
			return
		}

		outputRaw, err := json.Marshal(&data)
		if err != nil {
			log.Printf("error marshalling data \"%v\" to json: %s", data, err)
			w.WriteHeader(http.StatusInternalServerError)
			_, err := w.Write([]byte(http.StatusText(http.StatusInternalServerError)))
			if err != nil {
				log.Printf("error writing to output: %s", err)
			}
			return
		}

		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
		_, err = w.Write(outputRaw)
		if err != nil {
			log.Printf("error writing to output: %s", err)
		}
	})

	mux.HandleFunc("/modify.json", func(w http.ResponseWriter, r *http.Request) {
		dataRaw, err := io.ReadAll(r.Body)
		if err != nil {
			log.Printf("error reading request body: %s", err)
			w.WriteHeader(http.StatusBadRequest)
			_, err := w.Write([]byte(http.StatusText(http.StatusBadRequest)))
			if err != nil {
				log.Printf("error writing to output: %s", err)
			}
			return
		}
		defer r.Body.Close()

		data := modifyRequestData{}
		err = json.Unmarshal(dataRaw, &data)
		if err != nil {
			log.Printf("error unmarshalling string \"%s\": %s", dataRaw, err)
			w.WriteHeader(http.StatusBadRequest)
			_, err := w.Write([]byte(http.StatusText(http.StatusBadRequest)))
			if err != nil {
				log.Printf("error writing to output: %s", err)
			}
			return
		}

		var newValue float64
		err = db.Update(func(tx *bolt.Tx) error {
			bucket := tx.Bucket([]byte("output"))
			value, err := strconv.ParseFloat(string(bucket.Get([]byte(data.Category))), 64)
			if err != nil {
				log.Printf("loaded nil value from DB for key %s", data.Category)
			}

			if data.Operation == ModifyAdd {
				value += 1
			} else if data.Operation == ModifySub {
				value -= 1
			} else if data.Operation == ModifyAddHalf {
				value += 0.5
			} else if data.Operation == ModifySubHalf {
				value -= 0.5
			} else if data.Operation == ModifyAddQuarter {
				value += 0.25
			} else if data.Operation == ModifySubQuarter {
				value -= 0.25
			} else {
				return fmt.Errorf("unknown operation: %d", data.Operation)
			}
			newValue = math.Max(0, value)

			err = bucket.Put([]byte(data.Category), []byte(strconv.FormatFloat(newValue, 'G', -1, 64)))
			if err != nil {
				log.Printf("error saving modified value to DB: %s", err)
				return err
			}

			return nil
		})
		if err != nil {
			var statusCode int
			log.Printf("error updating database: %s", err)
			if strings.HasPrefix(err.Error(), "unknown operation") {
				statusCode = http.StatusBadRequest
			} else {
				statusCode = http.StatusInternalServerError
			}
			w.WriteHeader(statusCode)
			_, err := w.Write([]byte(http.StatusText(statusCode)))
			if err != nil {
				log.Printf("error writing to output: %s", err)
			}
			return
		}

		output := modifyResponseData{
			Category: data.Category,
			Value:    newValue,
		}
		outputRaw, err := json.Marshal(&output)
		if err != nil {
			log.Printf("error marshalling data \"%v\" to json: %s", output, err)
			w.WriteHeader(http.StatusInternalServerError)
			_, err := w.Write([]byte(http.StatusText(http.StatusInternalServerError)))
			if err != nil {
				log.Printf("error writing to output: %s", err)
			}
			return
		}

		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
		_, err = w.Write(outputRaw)
		if err != nil {
			log.Printf("error writing to output: %s", err)
		}
	})

	log.Fatal(server.ListenAndServe())
}
