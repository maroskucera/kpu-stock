module kpu-stock

go 1.18

require go.etcd.io/bbolt v1.3.6

require golang.org/x/sys v0.0.0-20200923182605-d9f96fdee20d // indirect

replace golang.org/x/sys v0.0.0-20200923182605-d9f96fdee20d => golang.org/x/sys v0.0.0-20220406163625-3f8b81556e12
