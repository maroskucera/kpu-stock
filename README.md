KPU Stock
=========

Simple app used to track stock output for KPU aid centre.

Licence
-------
KPU Stock is licensed under the
[Mozilla Public License, v. 2.0](https://mozilla.org/MPL/2.0/).


Author
------

Maros Kucera <maros (at) maroskucera (dot) com>
